function TestRun2(target,resources)
{
	this.target = target;
	this.spritesAr = new Array();
	this.resources = resources;
	this.setUpPreloader();
	this.objAr = new Array();
	this.objAr = [{x:185,y:200,color:'#7cc851',val:"A lot of attention is spent here"},{x:300,y:405,color:'#7cc851',val:"3 hrs a day consuming content on mobile"},{x:376,y:310,color:'#7cc851',val:"80x the number of times people unlock their phones in a day"},{x:473,y:285,color:'#7cc851',val:"1 in 5 mobile minutes spent on Facebook and Instagram "},{x:510,y:210,color:'#7cc851',val:"14X the number of times people check Facebook in a day"}]
}

TestRun2.prototype = {
	
	setUpPreloader: function()
	{
		this.keepAspectRatio = true;
		this.loadedGraphics = 0;
		canvas = document.getElementById("testCanvas2");
		this.stage = new createjs.Stage(canvas);
		this.stage.enableMouseOver(10);
		borderPadding = 10;

		var barHeight = 20;
		loaderColor = createjs.Graphics.getRGB(61, 92, 155);
		loaderBar = new createjs.Container();

		bar = new createjs.Shape();
		bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();

		imageContainer = new createjs.Container();
		imageContainer.x = this.resources.width;
		imageContainer.y = this.resources.height;

		loaderWidth = 300;
		this.stage.addChild(imageContainer);

		var bgBar = new createjs.Shape();
		var padding = 3
		bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);

		loaderBar.x = canvas.width - loaderWidth >> 1;
		loaderBar.y = canvas.height - barHeight >> 1;
		loaderBar.addChild(bar, bgBar);

		this.stage.addChild(loaderBar);

		manifest = 
		[
			{src: this.resources.background, id: "background"}
			//{src: "facebook_logo_animation.png", id: "logo"},
			//{src: "ball.png", id: "ball"}
			
		];
		this.map = new Array();
		this.preload = new createjs.LoadQueue(true);
       
		// Use this instead to use tag loading
		//preload = new createjs.PreloadJS(false);

		this.preload.on("progress", this.handleProgress,this);
		this.preload.on("complete", this.handleComplete,this);
		this.preload.on("fileload", this.handleFileLoad,this);
		//"course/en/animations/02_instagram_community/"
		this.preload.loadManifest(manifest,true);

		createjs.Ticker.setFPS(30);
	},
	
	handleProgress: function(event) {
		bar.scaleX = event.loaded * loaderWidth;
	},

	handleComplete: function(event) {
		var background = this.map[0]
		this.stage.addChild(background);
		loaderBar.visible = false;
		
	   	this.setupAnimation();
	   	//this.playAnimation();
	    
		
       	this.stage.update();
       	var fn = this.handleTick.bind(this);
       	var self = this;
       	createjs.Ticker.addEventListener("tick",fn);
	    //function handleTick(event) {
	    	//console.log("tick");

	    	//}
	},

    handleTick: function(event){
    	if(this.stage){this.stage.update()};
    },

	handleFileLoad: function(event) {
		this.map.push(new createjs.Bitmap(event.result))
		this.stage.update();
	},

    setupAnimation: function()
    {
    	
    	var gridItemsAr = new Array();
    	this.lines = new Array();
    	for(var i = 0 ;i<5;i++)
		{
			var shp = new createjs.Shape();
			this.lines.push(shp);
			

		}
    	for(var i=0;i<5;i++)
    	{
    	//this.setSmallGrid();
    	//this.animateToLargeGrid();
    	var temp = new createjs.Container();
    	var shape = new createjs.Shape();
    	shape.graphics.beginFill(this.objAr[i].color).drawCircle(0, 0, 50, 50);
    	temp.addChild(shape);
    	//
    	var text = new createjs.Text(this.objAr[i].val, "14px Arial","#ffffff");
 		text.x = 0;
 		text.textBaseline = "alphabetic";
 		text.x = -text.getMeasuredWidth()/2
 		text.y = 3;
    	temp.x = this.objAr[i].x;
    	temp.y = this.objAr[i].y;
    	if(i>0)
    	{
    		temp.alpha = 0;
    	}
    	if(i>0)
    	{
    		temp.x = this.objAr[i-1].x;
    		temp.y = this.objAr[i-1].y;
    		temp.id = i;
    		
	         

    	}
    	

    	temp.addChild(text);
    	this.spritesAr.push(temp);
    	 
    	this.stage.addChild(this.lines[i]);  
    	this.stage.update();

    	}
    	this.animateGraph();
    	this.stage.addChild(temp);
    },

    animateGraph: function()
    {
    	createjs.Tween.removeAllTweens();
    	for(var i=1;i<8;i++)
    	{
    	this.spritesAr[i].x = this.objAr[i-1].x;
		this.spritesAr[i].y = this.objAr[i-1].y;
		this.lines[i].graphics.clear();
		this.lines[i].alpha = 0;
		this.spritesAr[i].alpha = 0;
    	createjs.Tween.get(this.spritesAr[i])
	         .wait(20+(300*i))
			 .to({alpha:1,x:this.objAr[i].x,y:this.objAr[i].y,scaleY:1}, 400,createjs.Ease.backInOut)
	         .on("change", this.drawLine,this);
	    }
    },
    
    drawLine: function(evt)
	{
		var id = Number(evt.target._target.id);
		var myTarget = this.lines[id];
		myTarget.graphics.clear();
		myTarget.graphics.setStrokeStyle(2);
		myTarget.graphics.beginStroke("#edededed");
		

		myTarget.graphics.moveTo(this.objAr[id-1].x,this.objAr[id-1].y);
		myTarget.alpha = .3;
		myTarget.graphics.lineTo(evt.target._target.x,evt.target._target.y);
		this.stage.addChild(evt.target._target);
		if(id == 1)
		{
			this.stage.addChild(this.spritesAr[0]);
		}
		
	},
    
    
    
    resetAnimation: function()
    {
    	var fn = this.handleTick.bind(this);
       	createjs.Ticker.addEventListener("tick",fn);
    	this.animateGraph();
    	
    },

    animateToLargeGrid: function()
    {
    	var cnt = 0;
    	var hgt = 187;
    	var columns = 1;
    	for(var i=0;i<10;i++)
    	{
    		cnt+=1
    		var temp = this.spritesAr[i];
	    	
	    	var xLoc = 210+(155*cnt);
	    	var yLoc=(155*columns)-150;
	    	createjs.Tween.get(temp)
	         .wait(300*i)
	         .to({alpha:1,x:xLoc,y:yLoc,scaleX:1,scaleY:1}, 400,createjs.Ease.backInOut)
	    	if(cnt>=3){
	    		cnt=0;
	    		columns+=1;
	    	}
	    	
    	}
    },

	playAnimation: function()
	{
		/*
		var self = this;
		createjs.Tween.get(this.logo)
	         .wait(500)
	         .to({alpha:1,x:436,y:260}, 1000,createjs.Ease.elasticOut)
	         .call(function(){self.handleChange()})
	         createjs.Ticker.addEventListener("tick", handleTick);
	         function handleTick(event)
	          {
	          	console.log("tick");
	          	stage.update();

	          }
	    */
	},

	animationComplete: function(event)
	{
		
		//createjs.Ticker.on("tick", myFunction);
		//function myFunction(event) {
		   // event.remove();
		//}
		//debugger
		//createjs.Ticker.removeEventListener("tick", handleTick);
		var fn = this.handleTick.bind(this);
       	createjs.Ticker.removeEventListener("tick",self.handleTick);
	},

    
	

	resize: function(w,h)
	{
	
	var ow = this.resources.width; // your stage width
	var oh = this.resources.height; // your stage height

	if (this.keepAspectRatio == true)
	{
	    // keep aspect ratio
	    var scale = Math.min(w / ow, h / oh);
	    this.stage.scaleX = scale;
	    this.stage.scaleY = scale;

	   // adjust canvas size
	   this.stage.canvas.width = ow * scale;
	   this.stage.canvas.height = oh * scale;
	}
	
	this.stage.update()
	//this.replayAnimation();
	}

}
/*
function addCommas(nStr)
{
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
*/
